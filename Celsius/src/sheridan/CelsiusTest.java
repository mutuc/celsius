package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CelsiusTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	
	@Test
	public void testIsCelsiusNotValid() {
		int testInt = Celsius.convertFromFahrenheit(0);
		boolean testBool = false;
		if (testInt != 50) {
			testBool = true;
		}
		assertTrue("This is an exception", testBool);
		
	}
	
	@Test
	public void testCelsiusValid() {
		int testInt =	Celsius.convertFromFahrenheit(10);
		boolean testBool = true;
		if (testInt < 0) {
			testBool = false;
		}

				
		assertTrue("This is a proper value", testBool);
	}
	
	@Test
	public void testCelsiusOut() {
		int testInt = Celsius.convertFromFahrenheit(9);
		boolean testBool = true;
		if (testInt < 10) {
			testBool = false; 
		}
		assertFalse("Value is out of bounds for conversion", testBool);
	
	}	
	@Test
	public void testCelsiusIn() {
		int testInt =	Celsius.convertFromFahrenheit(10);
		boolean testBool = true;
		if (testInt < 0) {
			testBool = false;
		}

				
		assertTrue("This is a proper value", testBool);
	}

	
}
